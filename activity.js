//Fruits on sale

db.fruits.aggregate([
	{$match: {onSale: true}},
	{$count: "fruitsOnSale"}
]);

//Stock more than 20

db.fruits.aggregate([
	{$match: {stock: {$gte: 20}}},
	{$count: "enoughStock"}
]);

// Avg price of fruits on sale

db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplierId", avg_price: {$avg: "$price"}}}
]);

//Highest price of fruits per supplier

db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplierId", max_price: {$max: "$price"}}}
]);

// Lowest

db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplierId", min_price: {$min: "$price"}}}
]);